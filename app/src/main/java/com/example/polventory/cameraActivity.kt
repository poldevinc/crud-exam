package com.example.polventory

import android.Manifest
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Environment
import android.util.Log
import kotlinx.android.synthetic.main.activity_camera.*
import com.camerakit.CameraKitView
import java.io.File
import java.io.FileOutputStream
import android.Manifest.permission
import android.Manifest.permission.WRITE_EXTERNAL_STORAGE
import android.Manifest.permission.READ_EXTERNAL_STORAGE
import androidx.core.app.ActivityCompat
import android.content.pm.PackageManager
import android.app.Activity






class cameraActivity : AppCompatActivity() {

    // Storage Permissions
    private val REQUEST_EXTERNAL_STORAGE = 1
    private val PERMISSIONS_STORAGE =
        arrayOf<String>(Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE)

    private fun verifyStoragePermissions(activity: Activity) {
        // Check if we have write permission
        val permission = ActivityCompat.checkSelfPermission(activity, Manifest.permission.WRITE_EXTERNAL_STORAGE)

        if (permission != PackageManager.PERMISSION_GRANTED) {
            // We don't have permission so prompt the user
            ActivityCompat.requestPermissions(
                activity,
                PERMISSIONS_STORAGE,
                REQUEST_EXTERNAL_STORAGE
            )
        }
    }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_camera)

        verifyStoragePermissions(this)
        val sessionId = intent.getStringExtra("CATEGORY_ID")

        capture.setOnClickListener {
            camera.captureImage(CameraKitView.ImageCallback { cameraKitView, photo ->
                val savedPhoto = File(Environment.getExternalStorageDirectory(), "$sessionId.jpg")
                try {
                    val outputStream = FileOutputStream(savedPhoto.path)
                    outputStream.write(photo)
                    outputStream.close()
                    finish()
                } catch (e: java.io.IOException) {
                    e.printStackTrace()
                }
            })


        }
    }

    override fun onResume() {
        super.onResume()
        camera.onResume()
    }


    override fun onStart() {
        super.onStart()
        camera.onStart()
    }


    override fun onStop() {
        super.onStop()
        camera.onStop()
    }

    override fun onPause() {
        super.onPause()
        camera.onPause()
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        camera.onRequestPermissionsResult(requestCode,permissions,grantResults)
    }
}
