package com.example.poinventory

import androidx.room.*


@Database(entities = [Inventory::class,Category::class], version = 1)
abstract class AppDatabase : RoomDatabase() {
    abstract fun inventoryDao(): InventoryDao
    abstract fun categoryDao(): CategoryDao
}