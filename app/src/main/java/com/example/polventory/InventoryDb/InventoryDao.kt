package com.example.poinventory

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.room.*
import androidx.room.Update

@Dao
interface InventoryDao {

    @Query("SELECT * FROM inventory")
    fun getAllInventory(): LiveData<List<Inventory>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(item: Inventory)

    @Delete
    fun delete(item: Inventory)

    @Update
    fun update(item: Inventory)
    
}

@Dao
interface CategoryDao{

    @Query("SELECT * FROM category")
    fun getAllCategory(): LiveData<List<Category>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun AddCategorry(category: Category)

}