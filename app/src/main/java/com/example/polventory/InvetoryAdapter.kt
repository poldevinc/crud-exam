package com.example.polventory

import android.content.Context
import android.content.Intent
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.example.poinventory.Inventory
import kotlinx.android.synthetic.main.activity_add_item.*

class InvetoryAdapter: RecyclerView.Adapter<InvetoryAdapter.ViewHolder>()  {

    var items: List<Inventory> = emptyList()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): InvetoryAdapter.ViewHolder {
        val inflater = LayoutInflater.from(parent.context)
            .inflate(R.layout.row_item_layout,parent,false)


        return ViewHolder(inflater)
    }

    override fun getItemCount(): Int {
        return items.size
    }

    override fun onBindViewHolder(holder: InvetoryAdapter.ViewHolder, position: Int) {

        holder.name.text = items[position].product
        holder.quantity.text = items[position].category


        Glide
            .with(holder.imageView)
            .load(items[position].productPicture)
            .centerCrop()
            .diskCacheStrategy(DiskCacheStrategy.NONE)
            .skipMemoryCache(true)
            .into(holder.imageView)

        holder.cardvView.setOnClickListener {
            val intent = Intent(holder.context,AddEditItemActivity::class.java)
            intent.putExtra("ITEM_ID",AddEditItemActivity.EDIT)
            intent.putExtra("inventory",items[position])
            holder.context.startActivity(intent)
        }

    }

    fun setInventoryList(inventoryList: List<Inventory>){
        items = inventoryList
        notifyDataSetChanged()
    }

    fun getItemAt(position: Int) : Inventory{
        return items[position]
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView){

        var name: TextView = itemView.findViewById(R.id.name)
        var quantity: TextView = itemView.findViewById(R.id.category)
        var imageView : ImageView = itemView.findViewById(R.id.imageView)
        var cardvView : CardView = itemView.findViewById(R.id.cardvView)

        var context : Context = itemView.context
    }

}