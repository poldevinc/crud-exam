package com.example.polventory

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Environment
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.EditText
import android.widget.TextView
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.example.poinventory.Category
import kotlinx.android.synthetic.main.activity_add_item.*

import androidx.appcompat.app.AlertDialog
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.example.poinventory.Inventory
import java.io.File




class AddEditItemActivity : AppCompatActivity(){

    companion object {
        var EDIT = "0"
        var ADD = "1"
    }

    private lateinit var viewModel: InventoryViewModel
    lateinit var itemID : String
    lateinit var adapter :ArrayAdapter<String>
    private var inventory : Inventory = Inventory()
    val addItemPos = 1
    private var savedPhoto :File? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add_item)

        viewModel = ViewModelProviders.of(this).get(InventoryViewModel::class.java)


        initspiner()
        spinerListener()

        addPhoto.setOnClickListener {
            val intent = Intent(this,cameraActivity::class.java)
            intent.putExtra("CATEGORY_ID",spinner.selectedItem.toString())
            startActivity(intent)
        }

        save.setOnClickListener {

            inventory.product = itemName.text.toString()
            inventory.category = spinner.selectedItem.toString()
            inventory.productPicture = savedPhoto.toString()

            if (itemID == AddEditItemActivity.EDIT){
                viewModel.update(inventory)
            }else{
                viewModel.insert(inventory)
            }


            finish()
        }



    }

    private fun spinerListener(){
        spinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener{
            override fun onNothingSelected(parent: AdapterView<*>?) {

            }
            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                if (position == addItemPos){
                    customDialog()
                }
                if (position <= addItemPos ){
                    addPhoto.visibility = View.GONE
                    imageView.visibility = View.GONE
                }else{
                    addPhoto.visibility = View.VISIBLE
                    imageView.visibility = View.VISIBLE
                }
                updatePreviewImage()
            }

        }
    }

    private fun updatePreviewImage(){
        var selectedItem = spinner.selectedItem.toString()

        savedPhoto =  File(Environment.getExternalStorageDirectory(), "$selectedItem.jpg")

        Glide
            .with(imageView)
            .load(savedPhoto)
            .centerCrop()
           // .override(500,500)
            .diskCacheStrategy(DiskCacheStrategy.NONE)
            .placeholder(R.drawable.ic_mtrl_chip_close_circle)
            .skipMemoryCache(true)
            .into(imageView)
    }

    private fun initspiner() {
        adapter = ArrayAdapter<String>(
            this,
            android.R.layout.simple_spinner_item)


        viewModel.getAllCategory().observe(this, Observer {
            updateSpinner(it)
            initData()
        })

       adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        spinner.adapter = adapter

    }

    fun initData(){

        itemID = intent.getStringExtra("ITEM_ID")
        if (itemID == AddEditItemActivity.EDIT){
            val inventoryExtra = intent.getSerializableExtra("inventory")
            inventory = inventoryExtra as Inventory
            val spinnerPosition = adapter.getPosition(inventory.category)
            spinner.setSelection(spinnerPosition)
            itemName.setText(inventory.product)
            save.text = "Save Edit"
            ///updatePreviewImage()
        }

    }

    fun updateSpinner( it: List<Category>) {

        adapter.clear()

        if (it.isEmpty()){
            viewModel.insertCategory("Select a Category")
            viewModel.insertCategory("Add Category")
        }

        it.forEach {
            adapter.add(it.category)
        }

    }

    fun customDialog(){
        val dialogBuilder = AlertDialog.Builder(this)
        var alertDialog : AlertDialog? = null
        val inflater = this.layoutInflater
        val dialogView = inflater.inflate(R.layout.add_category_layout, null)

        dialogBuilder.setView(dialogView)

        val ok = dialogView.findViewById(R.id.ok) as TextView
        val cancel = dialogView.findViewById(R.id.cancel) as TextView
        val addItemEditText = dialogView.findViewById(R.id.addItemEditText) as EditText

        cancel.setOnClickListener {
            alertDialog!!.dismiss()
        }


        ok.setOnClickListener {

            viewModel.insertCategory(addItemEditText.text.toString())
            alertDialog!!.dismiss()
        }
        alertDialog = dialogBuilder.create()

        alertDialog.setOnDismissListener {
            spinner.setSelection(0)
        }

        alertDialog.show()
    }
}
