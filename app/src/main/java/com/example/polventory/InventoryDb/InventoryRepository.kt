package com.example.polventory

import android.content.Context
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.room.Room
import com.example.poinventory.AppDatabase
import com.example.poinventory.Category
import com.example.poinventory.Inventory


class InventoryRepository {


    private val DB_NAME = "db_inventory"
    private var appDatabase : AppDatabase? = null


    constructor(context:Context) {
        appDatabase = Room.databaseBuilder(context, AppDatabase::class.java, DB_NAME)
            .allowMainThreadQueries()   //Allows room to do operation on main thread
            .build()
    }

    fun insetItem(inventory : Inventory) {


      appDatabase!!.inventoryDao().insert(inventory)
    }

    fun getAllItem(): LiveData<List<Inventory>> {


        return appDatabase!!.inventoryDao().getAllInventory()
    }

    fun updateItem(inventory: Inventory){
        appDatabase!!.inventoryDao().update(inventory)
    }

    fun deleteItem(inventory: Inventory){
        appDatabase!!.inventoryDao().delete(inventory)
    }


    fun insertCategory(category: Category){
        appDatabase!!.categoryDao().AddCategorry(category)
    }

    fun getAllCategory() : LiveData<List<Category>>{
      return  appDatabase!!.categoryDao().getAllCategory()
    }

  /*  fun deleteItem(id:Int){
        val task = getTask(id)
        appDatabase!!.inventoryDao().delete(task.getValue())
    }

    fun getTask(id:Int):List<Inventory> {
        return noteDatabase.daoAccess().getTask(id)
    }
    fun getTasks():LiveData<List<Note>> {
        return noteDatabase.daoAccess().fetchAllTasks()
    }*/
}