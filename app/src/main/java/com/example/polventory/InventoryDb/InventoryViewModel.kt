package com.example.polventory

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import com.example.poinventory.Category
import com.example.poinventory.Inventory

class InventoryViewModel(application: Application) : AndroidViewModel(application) {
    var repository : InventoryRepository = InventoryRepository(application)


    fun insert(inventory: Inventory){
        repository.insetItem(inventory)
    }

    fun delete(inventory: Inventory){
        repository.deleteItem(inventory)
    }

    fun getAllItem () : LiveData<List<Inventory>>{
        return repository.getAllItem()
    }

    fun update (inventory: Inventory){
        repository.updateItem(inventory)
    }

    fun getAllCategory () : LiveData<List<Category>>{
        return repository.getAllCategory()
    }

    fun insertCategory(categoryString :String){
        var category = Category()
        category.category = categoryString
        repository.insertCategory(category)
    }
}