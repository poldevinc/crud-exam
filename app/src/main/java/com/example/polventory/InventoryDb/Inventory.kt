package com.example.poinventory

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import java.io.Serializable

@Entity(tableName = "inventory")
class Inventory : Serializable {

    @PrimaryKey(autoGenerate = true)
    var id = 0

    @ColumnInfo(name = "product")
    var product = ""

    @ColumnInfo(name = "category")
    var category = ""

    @ColumnInfo(name = "product_icture")
    var productPicture = ""

}

@Entity(tableName = "category")
class Category{

    @PrimaryKey(autoGenerate = true)
    var id = 0

    @ColumnInfo(name = "category")
    var category = ""
}