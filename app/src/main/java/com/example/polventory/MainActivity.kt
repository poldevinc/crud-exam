package com.example.polventory

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity;
import android.view.Menu
import android.view.MenuItem
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.content_main.*
import androidx.recyclerview.widget.ItemTouchHelper




class MainActivity : AppCompatActivity() {

    private lateinit var viewModel: InventoryViewModel
    private var adapter : InvetoryAdapter? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setSupportActionBar(toolbar)

        viewModel = ViewModelProviders.of(this).get(InventoryViewModel::class.java)



        fab.setOnClickListener { view ->
         /*   Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                .setAction("Action", null).show() */

            val intent = Intent(this,AddEditItemActivity::class.java)
            intent.putExtra("ITEM_ID",AddEditItemActivity.ADD)
            startActivity(intent)

        }

        recView.layoutManager = LinearLayoutManager(this)
        adapter = InvetoryAdapter()

        recView.adapter = adapter
        swipeListener()

    }

    private fun swipeListener(){

        val simpleItemTouchCallback =
            object : ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT or ItemTouchHelper.RIGHT) {
                override fun onMove(
                    recyclerView: RecyclerView,
                    viewHolder: RecyclerView.ViewHolder, target: RecyclerView.ViewHolder
                ): Boolean {
                    //                    final int fromPos = viewHolder.getAdapterPosition();
                    //                    final int toPos = viewHolder.getAdapterPosition();
                    //                    // move item in `fromPos` to `toPos` in adapter.
                    return true// true if moved, false otherwise
                }

                override fun onSwiped(viewHolder: RecyclerView.ViewHolder, swipeDir: Int) {
                    //Remove swiped item from list and notify the RecyclerView
                    viewModel.delete(adapter!!.getItemAt(viewHolder.adapterPosition))
                   // adapter!!.notifyItemRemoved(viewHolder.adapterPosition)
                }
            }
        val itemTouchHelper = ItemTouchHelper(simpleItemTouchCallback)
        itemTouchHelper.attachToRecyclerView(recView)
    }

    override fun onResume() {
        super.onResume()
        viewModel.getAllItem().observe(this, Observer {
            adapter!!.setInventoryList(it)
        })
    }


    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.menu_main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        return when (item.itemId) {
            R.id.action_settings -> true
            else -> super.onOptionsItemSelected(item)
        }
    }
}
